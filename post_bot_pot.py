#invoke required Python libraries
import numpy as np
import pandas as pd
import sys
import csv
import os
import shutil
from glob import glob
from pyeda.inter import *
import collections
from collections import Counter
import itertools
import scipy.stats
from scipy.stats import ttest_ind
import seaborn as sns
import matplotlib.pyplot as plt
from natsort import natsorted
import math
from matplotlib.gridspec import GridSpec
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.font_manager import FontProperties
import seaborn as sns
import datetime

sns.set()
sns.set_color_codes()
_ = plt.plot([0, 1], color="r")
sns.set_color_codes()
_ = plt.plot([0, 2], color="b")
sns.set_color_codes()
_ = plt.plot([0, 3], color="g")
sns.set_color_codes()
_ = plt.plot([0, 4], color="m")
sns.set_color_codes()
_ = plt.plot([0, 5], color="y")
#optional display adjustment
pd.set_option('display.width', None)

project_path = './../00/full/'

potential_files=[y for x in os.walk(project_path) for y in glob(os.path.join(x[0], '*peak_vectors_data.txt'))]
FULL_dfs1 = []
for filename in potential_files:
	print('Adding_' + filename + '_to_master_FULL_DataFrame.')
	temp_df= pd.read_csv(filename, sep='\t|:')
	temp_split=filename.split('/')
	temp_df['time_point']=int(temp_split[4])
	temp_df['tissue']=temp_split[5]
	temp_df['status']=temp_split[6]
	temp_df['replicate']=int(temp_split[7])
	FULL_dfs1.append(temp_df)

FULL_master = pd.concat(FULL_dfs1, ignore_index=True)
FULL_grouped = FULL_master.sort_values(['time_point', 'tissue', 'status', 'cluster', 'replicate'])
FULL_grouped.reset_index(drop=True, inplace=True)
#merge 31, 35, and 36DPI data to 30DPI in FULL data
FULL_grouped.loc[(FULL_grouped['time_point'] == 31) & (FULL_grouped['replicate'] == 1), 'replicate'] = 2
FULL_grouped.loc[(FULL_grouped['time_point'] == 31) , 'time_point'] = 30
FULL_grouped.loc[(FULL_grouped['time_point'] == 35) & (FULL_grouped['replicate'] == 1), 'replicate'] = 4
FULL_grouped.loc[(FULL_grouped['time_point'] == 35) & (FULL_grouped['replicate'] == 2), 'replicate'] = 5
FULL_grouped.loc[(FULL_grouped['time_point'] == 35) , 'time_point'] = 30
FULL_grouped.loc[(FULL_grouped['time_point'] == 36) & (FULL_grouped['replicate'] == 1), 'replicate'] = 7
FULL_grouped.loc[(FULL_grouped['time_point'] == 36) & (FULL_grouped['replicate'] == 2), 'replicate'] = 8
FULL_grouped.loc[(FULL_grouped['time_point'] == 36) , 'time_point'] = 30
FULL_grouped['time_point'].replace(to_replace=23, value=30, inplace=True)
#repeat baseline data for every time point on FULL dataset
FULL_baseline = {}
FULL_concat = [FULL_grouped[FULL_grouped['status'] != 'baseline']]
time_points = FULL_grouped['time_point'].unique()
for time_point in range(len(time_points)):
	tp_name = 'time_point%d' % (time_points[time_point])
	print('Adding baseline data to ' + tp_name + '.')
	FULL_baseline[tp_name] = FULL_grouped[FULL_grouped['status'] == 'baseline']
	FULL_concat.append(FULL_baseline[tp_name])

print('Aggregating FULL dataset.')
FULL_grouped = pd.concat(FULL_concat, axis=0)
FULL_final = FULL_grouped.sort_values(['time_point', 'tissue', 'status', 'cluster', 'replicate'])
FULL_final.reset_index(drop=True, inplace=True)
FULL_final=FULL_final[FULL_final['status'] != 'baseline']


potential_files=[y for x in os.walk(project_path) for y in glob(os.path.join(x[0], 'data.tsv'))]
FULL_dfs2 = []
for filename in potential_files:
	if 'sample' in filename:
		print('Adding_' + filename + '_to_master_FULL_DataFrame.')
		temp_df= pd.read_csv(filename, sep='\t')
		temp_split=filename.split('/')
		temp_df['time_point']=int(temp_split[4])
		temp_df['tissue']=temp_split[5]
		temp_df['status']=temp_split[6]
		temp_df['replicate']=int(temp_split[7])
		FULL_dfs2.append(temp_df)

FULL_master2 = pd.concat(FULL_dfs2, ignore_index=True)
FULL_grouped2 = FULL_master2.sort_values(['time_point', 'tissue', 'status', 'cluster', 'replicate'])
FULL_grouped2.reset_index(drop=True, inplace=True)
#merge 31, 35, and 36DPI data to 30DPI in FULL data
FULL_grouped2.loc[(FULL_grouped2['time_point'] == 31) & (FULL_grouped2['replicate'] == 1), 'replicate'] = 2
FULL_grouped2.loc[(FULL_grouped2['time_point'] == 31) , 'time_point'] = 30
FULL_grouped2.loc[(FULL_grouped2['time_point'] == 35) & (FULL_grouped2['replicate'] == 1), 'replicate'] = 4
FULL_grouped2.loc[(FULL_grouped2['time_point'] == 35) & (FULL_grouped2['replicate'] == 2), 'replicate'] = 5
FULL_grouped2.loc[(FULL_grouped2['time_point'] == 35) , 'time_point'] = 30
FULL_grouped2.loc[(FULL_grouped2['time_point'] == 36) & (FULL_grouped2['replicate'] == 1), 'replicate'] = 7
FULL_grouped2.loc[(FULL_grouped2['time_point'] == 36) & (FULL_grouped2['replicate'] == 2), 'replicate'] = 8
FULL_grouped2.loc[(FULL_grouped2['time_point'] == 36) , 'time_point'] = 30
FULL_grouped2['time_point'].replace(to_replace=23, value=30, inplace=True)
#repeat baseline data for every time point on FULL dataset
FULL_baseline2 = {}
FULL_concat2 = [FULL_grouped2[FULL_grouped2['status'] != 'baseline']]
time_points2 = FULL_grouped2['time_point'].unique()
for time_point in range(len(time_points2)):
	tp_name = 'time_point%d' % (time_points2[time_point])
	print('Adding baseline data to ' + tp_name + '.')
	FULL_baseline2[tp_name] = FULL_grouped2[FULL_grouped2['status'] == 'baseline']
	FULL_concat2.append(FULL_baseline2[tp_name])

print('Aggregating FULL dataset.')
FULL_grouped2 = pd.concat(FULL_concat2, axis=0)
FULL_final2 = FULL_grouped2.sort_values(['time_point', 'tissue', 'status', 'cluster', 'replicate'])
FULL_final2.reset_index(drop=True, inplace=True)

FULL_final2=FULL_final2[FULL_final2['status'] != 'baseline']

Boo_data1 = FULL_final.copy()
channel_columns = {}
channel_list = []

for channel in Boo_data1.iloc[:,4:15]:
   channel_columns[channel] = Boo_data1.loc[:,channel].values
   channel_list.append(channel)

Boo_data2 = Boo_data1.iloc[:,4:15].astype(int)
channel_list.sort()
Boo_data2 = Boo_data2[channel_list]
channel_list_update_dict = {'b220':'B220', 'cd45':'CD45', 'cd11b':'CD11b', 'cd11c':'CD11c', 'cd3e':'CD3e',
						'cd4':'CD4', 'cd49b':'CD49b', 'cd8':'CD8a', 'f480':'F480', 'ly6c':'Ly6C', 'ly6g':'Ly6G'}
Boo_data3 = Boo_data2.rename(columns=channel_list_update_dict)
Boo_data4 = pd.concat([Boo_data1.iloc[:,0:4], Boo_data3, Boo_data1.iloc[:,15:20]], axis=1)
#get unique Boolean vector heatmap
channel_list_update = list(channel_list_update_dict.values())
channel_list_update.sort()
unique_vectors = Boo_data4.drop_duplicates(channel_list_update)

bool_data_class = pd.read_csv('top_vector_stats_greg.csv')
del bool_data_class['ly6c:cd3e:cd11c:cd45:cd11b:cd4:ly6g:f480:cd49b:cd8:b220']
bool_data_class[['cell_type']]=bool_data_class[['cell_type']].fillna('unspecified')
bool_data_class = pd.concat([bool_data_class[channel_list],bool_data_class.iloc[:,11:15]], axis=1)
bool_data_class=bool_data_class.rename(columns=channel_list_update_dict)
bool_data_class=bool_data_class.fillna('New_class')

vector_classification={}

landmark_pops=[]

for index, row in bool_data_class.iterrows():
	if row['cell_type']!='unspecified':
		vector_classification[row['cell_type']]={}
		vector_classification[row['cell_type']]['lineage']=row['Lineage']
		vector_classification[row['cell_type']]['signature']=[]
		if row['Landmark']=='Y':
			landmark_pops.append(row['cell_type'])
		for i,num in enumerate(row[:-3]):
			if num!=0:
				vector_classification[row['cell_type']]['signature'].append(list(bool_data_class)[:-3][i])

del bool_data_class['Landmark']

classified = pd.merge(Boo_data4, bool_data_class, how='left', on=channel_list_update)

count2 = classified['cell_type'].value_counts()
percent_coverage = (sum(count2) - count2['unspecified'])/sum(count2) * 100
unspecified = classified[classified['cell_type'] == 'unspecified']

cnt = classified.groupby(['time_point', 'tissue', 'status', 'replicate', 'cell_type']).size()
cnt1 = cnt.unstack()
cnt2 = cnt1.replace(to_replace='NaN', value=0.0)
cnt3 = cnt2.stack().astype(int)
ave = cnt3.groupby(level=['time_point', 'tissue', 'status', 'replicate']).apply( lambda x: (x / x.sum())*100)
g1 = ave.unstack()
g2 = g1.reset_index()
g3 = g2.iloc[:,:4]
g4 = g3[g3['status'] == 'naive']
g5 = g4.replace(to_replace='naive', value='gl261')
g6 = pd.concat([g4, g5], axis=0).reset_index(drop=True)
g7 = pd.merge(g6, g2, how='left')
g8 = g7.replace(to_replace='NaN', value=0.0)
FULL_gl261 = g8.loc[g8['status'] == 'gl261'].reset_index(drop=True)
FULL_gl261 = pd.melt(FULL_gl261, ['time_point', 'tissue', 'status', 'replicate'])
FULL_gl261 = FULL_gl261.rename(columns={'value': 'gl261_percent'})
FULL_naive = g8.loc[g8['status'] == 'naive'].reset_index(drop=True)
FULL_naive = pd.melt(FULL_naive, ['time_point', 'tissue', 'status', 'replicate'])
FULL_naive = FULL_naive.rename(columns={'value': 'naive_percent'})
FULL_t_input = pd.concat([FULL_gl261, FULL_naive], axis = 1)
t_left = FULL_t_input.iloc[:,0:5]
t_right = FULL_t_input[['gl261_percent', 'naive_percent']]
FULL_t_input = pd.concat([t_left, t_right], axis=1)
FULL_t_input = FULL_t_input.drop(['status','replicate'], axis=1)
FULL_t_stats_list = []

for i, group in FULL_t_input.groupby(['time_point', 'tissue', 'cell_type']):
	statistics = ttest_ind(group['gl261_percent'], group['naive_percent'], axis=0, equal_var=True, nan_policy='propagate')
	FULL_t_stats_list.append(statistics)

FULL_statistics = pd.DataFrame(FULL_t_stats_list)
FULL_t_output = FULL_t_input.groupby(['time_point', 'tissue', 'cell_type']).sum()
FULL_t_output.reset_index(drop=False, inplace=True)
FULL_t_output = FULL_t_output.drop(['gl261_percent', 'naive_percent'], axis =1)
FULL_t_output = pd.concat([FULL_t_output, FULL_statistics], axis = 1)
FULL_t_means = FULL_t_input.groupby(['time_point', 'tissue', 'cell_type']).mean()
FULL_t_means = FULL_t_means.reset_index(drop = False, inplace = False)
x_t = FULL_t_means
y_t = FULL_t_output.iloc[:, 3:5]
t_dfs = [x_t, y_t]
FULL_sig_dif_all = pd.concat(t_dfs,  axis = 1)
FULL_sig_dif_all = FULL_sig_dif_all.sort_values(by =['pvalue'])
FULL_sig_dif_all = FULL_sig_dif_all.replace(to_replace='NaN', value=0.0)
FULL_sig_dif = FULL_sig_dif_all[FULL_sig_dif_all['pvalue'] <= 0.05]
FULL_sig_dif = FULL_sig_dif[abs(FULL_sig_dif['statistic']) > 2.131]
FULL_sig_dif.reset_index(drop=True, inplace=True)
FULL_sig_dif = FULL_sig_dif.sort_values(['time_point', 'tissue', 'cell_type', 'pvalue'])
FULL_sig_dif.reset_index(drop=True, inplace=True)

print('Mapping phenograph clusters to Boolean classifier.')
cluster_dist = classified.groupby(['cluster', 'cell_type']).size()
consensus_Boo = cluster_dist.unstack(level=0).idxmax()
classified_map = pd.merge(classified, pd.DataFrame({'consensus': consensus_Boo}), left_on='cluster', right_index=True)
consensus_Boo = pd.DataFrame(consensus_Boo)
consensus_Boo.columns = ['consensus']
consensus_Boo.reset_index(level=0, inplace=True)
consensus_Boo['cluster'] = consensus_Boo.cluster.map(str) + ' ' + '(' + consensus_Boo.consensus + ')'
consensus_Boo = consensus_Boo.drop('consensus', axis=1)
consensus_Boo = consensus_Boo['cluster']
cluster_dict = consensus_Boo.to_dict()
#combine classified Boolean DataFrame with continuous expression values
print('Combining Phenograph and Boolean classifier results into overall DataFrame.')
expression_values = FULL_final2.loc[:, channel_list + ['fsc', 'ssc', 'index']]
overall = pd.merge(classified_map, expression_values, on='index')
overall.drop(['fsc_x', 'ssc_x'], axis=1, inplace=True)
overall = overall.rename(index=str, columns={'fsc_y': 'fsc', 'ssc_y': 'ssc'})
overall_cols = ['time_point', 'tissue', 'status', 'replicate', 'cluster',
				'B220', 'CD11b', 'CD11c', 'CD3e', 'CD4', 'CD45', 'CD49b',
				'CD8a', 'F480', 'Ly6C', 'Ly6G', 'cell_type',
				'consensus', 'fsc', 'ssc', 'b220', 'cd11b', 'cd11c', 'cd3e',
				'cd4', 'cd45', 'cd49b', 'cd8', 'f480', 'ly6c', 'ly6g', 'index']
overall = overall[overall_cols]
overall = overall.rename(index=str, columns={'cd8': 'cd8a'})
color_dict = dict(zip(FULL_sig_dif['tissue'].unique(), ['r', 'b', 'g', 'm', 'y']))
FULL_sig_dif['dif'] = (FULL_sig_dif['gl261_percent']-FULL_sig_dif['naive_percent']).astype(float)

FULL_final_clusterdict = FULL_final.replace({'cluster': cluster_dict})
FULL_final_clusterdict = FULL_final_clusterdict.sort_values(['cluster', 'status'], ascending=[True, False])

for channel in channel_list:
	FULL_final_clusterdict_filt = FULL_final_clusterdict[(FULL_final_clusterdict[channel] > -3.5) & (FULL_final_clusterdict[channel] < 3.5)]

classified['cell_type'] = classified['cell_type'].str.replace('/', '-')
cell_type_stats={}
for i in set(classified['cell_type']):
	cell_type_stats[i]={}

frames = []
channel_list_cd8a = channel_list.copy()
channel_list_cd8a[7] = 'cd8a'
ov = overall[channel_list_cd8a + ['fsc', 'ssc', 'cell_type', 'status']][overall['cell_type'] != 'unspecified']
ov = ov.sort_values(['cell_type', 'status'], ascending=[True, False])
for name, group in ov.groupby(['cell_type']):
	#print('Extracting protein expression data for the ' + name + ' cell Boolean vector.')
	group.columns = [str(col) + '_' + name for col in group.columns]
	data = pd.melt(group)
	for channel in channel_list_cd8a:
		x = data['variable'][data['variable'] == channel + '_' + name].reset_index(drop=True)
		y = data['value'][data['variable'] == channel + '_' + name].reset_index(drop=True)
		hue = data['value'][data['variable'] == 'status' + '_' + name].reset_index(drop=True)
		m = pd.concat([x, y, hue], axis=1)
		m.columns = ['marker', 'value', 'status']
		frames.append(m)
		overall_dataframe_FULL = pd.concat(frames)
		
print('Splitting marker column into channel and cell_type columns in overall_dataframe_FULL')
split1 = overall_dataframe_FULL['marker'].str.split('_', expand=True)
to_concat = [split1, overall_dataframe_FULL.iloc[:,1:]]
foo = pd.concat(to_concat,  axis=1)
foo.columns = ['channel', 'cell_type', 'value', 'status']
ov=ov[ov['status'] != 'baseline']


for cell in foo['cell_type'].unique():
	data = foo[foo['cell_type'] == cell].copy()
	data = data[data['status'] != 'baseline']
	data['value'] = data['value'].astype('float')
	cell_type_stats[cell]['violin']=data
	cell_type_stats[cell]['violin-fsc-ssc']=ov[ov['cell_type'] == cell]

FULL_sig_dif_all_clusterdict = FULL_sig_dif_all.replace({'cluster': cluster_dict})
FULL_sig_dif_all_clusterdict['dif'] = (FULL_sig_dif_all_clusterdict['gl261_percent']-\
										FULL_sig_dif_all_clusterdict['naive_percent']).astype(float)
for name, group in FULL_sig_dif_all_clusterdict.groupby(['cell_type']):
	group = group.pivot_table(index='tissue', columns='time_point', values='dif')
	cell_type_stats[name]['sig_diff_all']=group

FULL_sig_dif_all_clusterdict['ratio'] = np.log2(FULL_sig_dif_all_clusterdict['gl261_percent']/\
										FULL_sig_dif_all_clusterdict['naive_percent']).astype(float)

FULL_sig_dif_all_clusterdict['ratio'].replace(to_replace=['Nan'], value=0.0, inplace=True)

for name, group in FULL_sig_dif_all_clusterdict.groupby(['cell_type']):
	group = group.pivot_table(index='tissue', columns='time_point', values='ratio')
	cell_type_stats[name]['sig_rat_all']=group

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.offsetbox import AnchoredText
import matplotlib.gridspec as gridspec

new_style = {'grid': False}
matplotlib.rc('axes', **new_style)

for name, group in classified.groupby(['cell_type']):
	data = group['tissue'].value_counts()
	total = group['tissue'].value_counts().sum()
	percent = (total/(len(classified.index))*100)
	cell_type_stats[str(name)]['data']=data
	cell_type_stats[str(name)]['percent']=percent


pair_grid_input = FULL_sig_dif.copy()

pair_grid_input['weighted_fold_change'] = np.log2((0.005 + pair_grid_input['gl261_percent'])/\
											(0.005 + pair_grid_input['naive_percent']))
pair_grid_input['name'] = pair_grid_input['tissue'].map(str) + '_' + pair_grid_input['cell_type']
time_sig = pair_grid_input.groupby(['tissue', 'cell_type']).size()
time_sig = pd.DataFrame(time_sig)
time_sig = time_sig.reset_index()
time_sig['name'] = time_sig['tissue'].map(str) + '_' + time_sig['cell_type']
time_sig = time_sig.drop(['tissue', 'cell_type'], axis=1)
time_sig.columns = ['time_sig', 'name']
pair_grid_input = pd.merge(pair_grid_input, time_sig, on='name')
pair_grid_input['name'] = pair_grid_input['time_point'].map(str) + '_' + pair_grid_input['name']
pair_grid_input = pair_grid_input.drop(['time_point', 'tissue', 'cell_type', 'gl261_percent', 'naive_percent'], axis=1)
pair_grid_input['priority_score'] = (((1-pair_grid_input['pvalue'])**3) * (abs(pair_grid_input['weighted_fold_change'] + pair_grid_input['dif']))) + pair_grid_input['time_sig']
pair_grid_col_order = ['name', 'priority_score', 'weighted_fold_change', 'dif', 'pvalue', 'time_sig', 'statistic']
pair_grid_input = pair_grid_input[pair_grid_col_order]
pair_grid_input.sort_values('priority_score', ascending=False, inplace=True)

def sort_preserve_order(seq):
	seen = set()
	seen_add = seen.add
	return [x for x in seq if not (x in seen or seen_add(x))]

def make_ticklabels_invisible(fig):
	for i, ax in enumerate(fig.axes):
		#ax.text(0.5, 0.5, "ax%d" % (i+1), va="center", ha="center")
		for tl in ax.get_xticklabels() + ax.get_yticklabels():
			tl.set_visible(False)

temp_sorted_grid=list(pair_grid_input['name'])
rank_sorted_grid = sort_preserve_order([a.split('_')[-1] for a in temp_sorted_grid])

order_of_dict = sorted(cell_type_stats,key=lambda x:cell_type_stats[x]['percent'], reverse=True)
order_of_dict.remove('unspecified')
rank_sorted_grid.remove('unspecified')
#order_of_dict.remove('pDC')
#rank_sorted_grid.remove('pDC')

save_file = PdfPages('BirdsEyeView_'+datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S")+'.pdf')
plt.clf()
fig = plt.figure(figsize=(150, 100))
the_grid = gridspec.GridSpec(int(len(cell_type_stats.keys())/8)+1, 8, wspace=0.2, hspace=0.2)
max_radius=math.sqrt(cell_type_stats[order_of_dict[0]]['percent'])/5

for i in range(len(order_of_dict)):
	data_to_consider=cell_type_stats[order_of_dict[i]]
	radius = math.sqrt(data_to_consider['percent'])/5
	inner=gridspec.GridSpecFromSubplotSpec(7, 4, subplot_spec=the_grid[i], wspace=0.05, hspace=0.05)
	ax0 = plt.Subplot(fig, inner[0,:])
	for tl in ax0.get_xticklabels() + ax0.get_yticklabels():
		tl.set_visible(False)
	if order_of_dict[i] in landmark_pops:
		ax0.text(0.9,0.8, 'landmark population', horizontalalignment='right', verticalalignment='center', fontsize=18, color='blue')
		ax0.text(0.9,0.5, 'Signature: '+','.join(vector_classification[order_of_dict[i]]['signature']), horizontalalignment='right', verticalalignment='center', fontsize=15)
	if vector_classification[order_of_dict[i]]['lineage'] =="Myeloid":
		ax0.text(1,0.8, 'M', horizontalalignment='right', verticalalignment='top', fontsize=40,bbox={'facecolor':'khaki', 'alpha':0.5, 'pad':10})
	if vector_classification[order_of_dict[i]]['lineage'] =="Lymphoid":
		ax0.text(1,0.8, 'L', horizontalalignment='right', verticalalignment='top', fontsize=40,bbox={'facecolor':'dodgerblue', 'alpha':0.5, 'pad':10})
	if vector_classification[order_of_dict[i]]['lineage'] =="Other":
		ax0.text(1,0.8, 'O', horizontalalignment='right', verticalalignment='top', fontsize=40,bbox={'facecolor':'grey', 'alpha':0.5, 'pad':10})
	ax0.text(0.01,0.85, order_of_dict[i].replace('neg',' - '), fontsize= 40)
	if order_of_dict[i] in rank_sorted_grid:
		ax0.text(0.5,0.5, "Priority Rank", horizontalalignment='center', verticalalignment='center', fontsize= 20, color="blue")
		ax0.text(0.5,0.85, str(rank_sorted_grid.index(order_of_dict[i])+1), horizontalalignment='center', verticalalignment='center', fontsize= 50, color="red")
	ax0.set_axis_bgcolor('white')
	fig.add_subplot(ax0)
	ax1 = plt.Subplot(fig, inner[1:-3,:-3])
	ax1.pie([100,0], radius = radius, shadow=False, startangle=90)
	ax1.add_patch(matplotlib.patches.Rectangle((-max_radius*1.2, -max_radius*1.2), max_radius*1.2*2, max_radius*1.2*2, fill=None, alpha=1))
	ax1.add_artist(AnchoredText(str(round(data_to_consider['percent'],2))+'%', loc=1))
	ax1.add_artist(AnchoredText("pop.size", loc=2))
	fig.add_subplot(ax1)
	ax2 = plt.Subplot(fig, inner[1:-3,-3])
	labels = data_to_consider['data'].index.tolist()
	colors = [color_dict[x] for x in labels]
	ax2.pie(data_to_consider['data'],shadow=False, colors=colors, startangle=90)
	ax2.set_title("tissue dist.", fontsize= 20)
	fig.add_subplot(ax2)
	ax3 = plt.Subplot(fig, inner[1:-3,-2])
	g = sns.heatmap(data_to_consider['sig_rat_all'], square=True, ax=ax3, vmin=-4.0, vmax=4.0,cbar=False)
	g.set_ylabel('')
	g.set_xlabel('')
	for item in g.get_yticklabels():
		item.set_rotation(0)
	for item in g.get_xticklabels():
		item.set_rotation(90)
	ax3.set_title("log2(t/n)", fontsize= 20)
	fig.add_subplot(ax3)
	ax4 = plt.Subplot(fig, inner[1:-3,-1:])
	g = sns.heatmap(data_to_consider['sig_diff_all'], square=True, ax=ax4, vmin=-10, vmax=10,cbar=False)
	g.set_ylabel('')
	g.set_xlabel('')
	for item in g.get_yticklabels():
		item.set_rotation(0)
	for item in g.get_xticklabels():
		item.set_rotation(90)
	ax4.set_title("t-n", fontsize= 20)
	fig.add_subplot(ax4)
	ax5 = plt.Subplot(fig, inner[-3:,0])
	df = data_to_consider['violin-fsc-ssc']
	temp=df[df.columns[-4:]].copy(deep='True')
	temp_ssc=temp[['ssc','cell_type','status']].copy(deep='True')
	temp_fsc=temp[['fsc','cell_type','status']].copy(deep='True')
	temp_ssc['channel_name']='ssc'
	temp_fsc['channel_name']='fsc'
	temp_ssc= temp_ssc.rename(index=str, columns={"ssc": "data"})
	temp_fsc = temp_fsc.rename(index=str, columns={"fsc": "data"})
	final_temp = temp_fsc.append(temp_ssc, ignore_index=True)
	final_temp['data'] = final_temp['data'].astype('float')
	g = sns.violinplot(x='channel_name', y='data', hue='status',
	data=final_temp, split=True, inner='quart', ax=ax5,
	palette={'gl261': 'b', 'naive': 'y'})
	sns.despine(left=True)
	for item in g.get_xticklabels():
		item.set_rotation(90)
	ax5.set_ylim(-0, 250000)
	ax5.axhline(y=0.0, xmin=0, xmax=5.0, linewidth=1, linestyle='dashed', color = 'k', alpha=0.7)
	fig.add_subplot(ax5)
	ax6 = plt.Subplot(fig, inner[-3:,1:])
	g = sns.violinplot(x='channel', y='value', hue='status',
		data=data_to_consider['violin'], split=True, inner='quart', ax=ax6,
		palette={'gl261': 'b', 'naive': 'y'},
		order=['cd45', 'cd3e', 'cd4', 'cd8a', 'b220', 'cd11c', 'cd11b', 'ly6c', 'f480', 'ly6g', 'cd49b'])
	sns.despine(left=True)
	for item in g.get_xticklabels():
		item.set_rotation(90)
	ax6.set_ylim(-3.5, 3.5)
	ax6.axhline(y=0.0, xmin=0, xmax=5.0, linewidth=1, linestyle='dashed', color = 'k', alpha=0.7)
	fig.add_subplot(ax6)
save_file.savefig(fig,bbox_inches='tight')
save_file.close()