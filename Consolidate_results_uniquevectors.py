import numpy as np
import csv
import matplotlib.pyplot as plt
import scipy
import itertools
from matplotlib.backends.backend_pdf import PdfPages
import sys
import os
from glob import glob

filelocation = sys.argv[-1]
filelocation ='./../00/full/'

looking_for_or_criterion =['marrow','spleen','blood','nodes','thymus']

all_files_peakvectors=[y for x in os.walk(filelocation) for y in glob(os.path.join(x[0], '*-unique_peakvectors.txt'))]

output_file_info_each_unique="Unique_vectors_per_sample.txt"
output_file_info_complete_unique="Unique_vectors_overall.txt"
Global_list=[]
Global_list_only_tuples=[]

for i in all_files_peakvectors:
	if any(word in i for word in looking_for_or_criterion):
		print (i)
		with open(i) as f:
			data=f.readlines()	
			if len(Global_list)==0:
				Global_list.append((data[0].strip(),"sample_info"))	
			data=data[1:]
		for num,iii in enumerate(data):
			data[num]=iii.strip()
		for iii in data:
			Global_list.append((iii,os.path.dirname(os.path.dirname(i))[5:].replace('/','-')))
			Global_list_only_tuples.extend(data)

detected_peaks_file_write=open(output_file_info_complete_unique,'w')
detected_peaks_file_write.write(Global_list[0][0]+'\n')
for iii in set(Global_list_only_tuples):
	detected_peaks_file_write.write(iii+'\n')
detected_peaks_file_write.close()

detected_peaks_file_write=open(output_file_info_each_unique,'w')
for iii in Global_list:
	detected_peaks_file_write.write(iii[0]+'\t'+iii[1]+'\n')
detected_peaks_file_write.close()
	



