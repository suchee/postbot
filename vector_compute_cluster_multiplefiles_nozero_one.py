import numpy as np
import csv
import matplotlib.pyplot as plt
import itertools
from matplotlib.backends.backend_pdf import PdfPages
import sys
import os
from glob import glob

filelocation = sys.argv[-1]
filelocation ='./../00/full/'

looking_for_and_criterion = ['sample','data.tsv']
looking_for_or_criterion =['marrow','spleen','blood','nodes','thymus']
not_looking_for=['baseline']

def discretize(list, value):
    #value-float(value)
	for num,i in enumerate(list):
		if num!=len(list)-1:
			if value<=i:
				return num+1
		if num==len(list)-1:
			if value<=i:
				return num+1
			if value>i:
				return num+2

with open('trough_info.txt') as f:
	trough_file=f.readlines()


info_troughs={}

for i in trough_file:
	info_troughs[i.split('\t')[0]]=[float(x) for x in i.split('\t')[1].strip().split(',')[:-1]]

potential_files=[y for x in os.walk(filelocation) for y in glob(os.path.join(x[0], '*.tsv'))]

for i in potential_files:
	if all(word in i for word in looking_for_and_criterion):
		if any(word in i for word in looking_for_or_criterion):
			data=np.loadtxt(open(i,"rb"),delimiter="\t",skiprows=1) 
			with open(i) as f:
				header_columns=next(csv.reader(f, delimiter='\t'))
			Peak_tuple_print=[]
			for ii in data:
				local_tuple=""
				for num,j in enumerate(ii):
						if header_columns[num] in info_troughs.keys():
							if discretize(info_troughs[header_columns[num]],j)==1:
								local_tuple=local_tuple+str(0)
							elif not discretize(info_troughs[header_columns[num]],j):
								local_tuple=local_tuple+str(0)
							else:
								local_tuple=local_tuple+str(discretize(info_troughs[header_columns[num]],j)-1)
				Peak_tuple_print.append(local_tuple)
			detected_vectors_file_write=open(os.path.dirname(i)+'/'+os.path.dirname(os.path.dirname(i))[5:].replace('/','-')+'-peak_vectors_data.txt','w')
			detected_vectors_file_write_unique=open(os.path.dirname(i)+'/'+os.path.dirname(os.path.dirname(i))[5:].replace('/','-')+'-unique_peakvectors.txt','w')
			detected_vectors_file_write.write("cluster\tindex\t"+':'.join(header_columns[1:-2])+"\n")
			for num,iii in enumerate(data):	
				detected_vectors_file_write.write(str(int(iii[0]))+'\t'+str(int(iii[-1]))+'\t'+":".join(Peak_tuple_print[num])+'\n')
			detected_vectors_file_write.close()
			detected_vectors_file_write_unique.write(':'.join(header_columns[1:-2])+"\n")
			for iii in set(Peak_tuple_print):
				detected_vectors_file_write_unique.write(":".join(iii)+'\n')
			detected_vectors_file_write_unique.close()
			print ("writing vectors of file "+i + " with " + str(np.loadtxt(open(i,"rb"),delimiter="\t",skiprows=1).shape[0]) + " cells")
