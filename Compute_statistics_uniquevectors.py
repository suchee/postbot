import sys
import os
from glob import glob

filelocation = sys.argv[-1]
filelocation ='./../00/full/'
looking_for_tissue =['marrow','spleen','blood','nodes','thymus']
looking_for_time =['00','07','14','23','31','35','36']
looking_for_condition=['gl261','naive']
looking_for_replicate=[str(i) for i in range(1,9)]
total_number_of_Clusters=41
input_file_info_complete_unique="Unique_vectors_overall.txt"
#not_loooking = ['baseline']
not_loooking = []

all_files_peakvectors=[y for x in os.walk(filelocation) for y in glob(os.path.join(x[0], '*-peak_vectors_data.txt'))]
with open(input_file_info_complete_unique,'r') as f:
	unique_vectors=f.readlines()

header_columns = unique_vectors[0].strip()
unique_vectors=[f.strip() for f in unique_vectors]
unique_vectors=unique_vectors[1:]

#use if you want to remove sc columns
unique_vectors_no_sc=[]
header_columns = header_columns.strip().split(':')
indices_to_remove = [i for i, s in enumerate(header_columns) if 'sc' in s]
for i in unique_vectors:
	unique_vectors_no_sc.append(":".join([k for j, k in enumerate(i.split(':')) if j not in indices_to_remove]))
unique_vectors_no_sc=[unique_vectors_no_sc[0]]+list(set(unique_vectors_no_sc[1:]))
Stat_unique_vectors={}
for i in unique_vectors_no_sc:
	Stat_unique_vectors[i]={}
	Stat_unique_vectors[i]['overall']=0
	for j in looking_for_time:
		Stat_unique_vectors[i]['day'+j]=0
	for j in looking_for_tissue:
		Stat_unique_vectors[i][j]=0
	for j in looking_for_condition:
		Stat_unique_vectors[i][j]=0
	for j in [str(k) for k in range(total_number_of_Clusters)]:
		Stat_unique_vectors[i][j]=0
	for j in looking_for_replicate:
		Stat_unique_vectors[i]['replicate'+j]=0
for i in all_files_peakvectors:
	if not any(word in i for word in not_loooking):
		keys_file=set((looking_for_condition+looking_for_time+looking_for_tissue+looking_for_replicate))&set(i.split('/'))
		with open(i) as f:
			data=f.readlines()	
			data=data[1:]
			for j in data:
				k=j.split('\t')
				Stat_unique_vectors[":".join([kk for kkk, kk in enumerate(k[2].strip().split(':')) if kkk not in indices_to_remove])]['overall']+=1
				Stat_unique_vectors[":".join([kk for kkk, kk in enumerate(k[2].strip().split(':')) if kkk not in indices_to_remove])][k[0]]+=1
				for jj in keys_file:
					if jj in looking_for_time:
						Stat_unique_vectors[":".join([kk for kkk, kk in enumerate(k[2].strip().split(':')) if kkk not in indices_to_remove])]['day'+jj]+=1
					elif jj in looking_for_replicate:
						Stat_unique_vectors[":".join([kk for kkk, kk in enumerate(k[2].strip().split(':')) if kkk not in indices_to_remove])]['replicate'+jj]+=1
					else:
						Stat_unique_vectors[":".join([kk for kkk, kk in enumerate(k[2].strip().split(':')) if kkk not in indices_to_remove])][jj]+=1

f=open("statistics_vectors_wo_sc.txt",'w')
f.write(":".join([kk for kkk, kk in enumerate(header_columns) if kkk not in indices_to_remove])+"\t")

for j in (looking_for_condition+looking_for_time+looking_for_tissue+looking_for_replicate):
	if j in looking_for_time:
		f.write('day'+j+'\t')
	elif j in looking_for_replicate:
		f.write('replicate'+j+'\t')
	else:
		f.write(j+'\t')

for j in [str(k) for k in range(total_number_of_Clusters)]:
	f.write("Cluster "+j+'\t')
f.write('\n')

for i in Stat_unique_vectors:
	f.write(i+'\t')
	for j in (looking_for_condition+looking_for_time+looking_for_tissue+looking_for_replicate):
		if j in looking_for_time:
			f.write(str(Stat_unique_vectors[i]['day'+j])+'\t')
		elif j in looking_for_replicate:
			f.write(str(Stat_unique_vectors[i]['replicate'+j])+'\t')
		else:
			f.write(str(Stat_unique_vectors[i][j])+'\t')
	for j in [str(k) for k in range(total_number_of_Clusters)]:
		f.write(str(Stat_unique_vectors[i][j])+'\t')
	f.write('\n')
f.close()
	
f=open("top_vector_stats.csv",'w')
f.write(":".join([kk for kkk, kk in enumerate(header_columns) if kkk not in indices_to_remove])+','+ ",".join([kk for kkk, kk in enumerate(header_columns) if kkk not in indices_to_remove])+", count\n")
for s in sorted(Stat_unique_vectors.iteritems(), key=lambda (x, y): y['overall'], reverse=True):
	f.write(s[0]+','+','.join(s[0].split(':'))+','+str(s[1]['overall'])+'\n')
f.close()


